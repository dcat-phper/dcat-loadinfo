<?php

use Yjn\LoadInfo\Http\Controllers;
use Illuminate\Support\Facades\Route;

Route::get('loadinfo', Controllers\LoadinfoController::class.'@index');