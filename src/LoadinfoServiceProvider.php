<?php

namespace Yjn\LoadInfo;

use Dcat\Admin\Extend\ServiceProvider;
use Dcat\Admin\Admin;
use \Dcat\Admin\Form\Field\Select;

class LoadinfoServiceProvider extends ServiceProvider
{
	protected $js  = [
		'js/index.js',
	];
	protected $css = [
		'css/index.css',
	];
	
	public function register()
	{
		//
	}
	
	public function init()
	{
		parent::init();
		$this->display();
		
	}
	
	public function settingForm()
	{
		return new Setting($this);
	}
	
	
	protected function display()
	{
		
		Select::macro('loadinfo', function (...$par) {
			$sourceUrl = admin_url($par[0]);
			$classInfo = $par[1];
			
			$field = array_map(function ($val) {
				return static::FIELD_CLASS_PREFIX . $val;
			}, $classInfo);
			
			$field = json_encode($field);
			
		
			$script = <<<JS
	
		$(document).off('change', "{$this->getElementClassSelector()}");
		$(document).on('change', "{$this->getElementClassSelector()}", function () {
		
		var field = {$field};
	    if (String(this.value) !== '0' && ! this.value) {
			$.each(field, function(key,val){
			    $("."+val).val('');
			});
	        return;
	    }
    
	    $.ajax("$sourceUrl?q="+this.value).then(function (data) {
			$.each(data, function(key,val){
				console.log(val);
			    $(".field_"+key).val(val);
			});
	    });
		});
		$("{$this->getElementClassSelector()}").trigger('change');
JS;
			
			Admin::script($script);
			return $this;
		});
		
		
	}
}
