# Dcat Admin Extension
#### 用途 调用form->select 联动输出form->text()文本框的内容

##### 通过下拉选项填充input输入框的默认值

##### 为select组件增强loadinfo 方法

```PHP
     		$form->select('buildcontract_id')->options(function ($id) {
		        $info = \App\Models\BuildcontractLease::where('id',$id)->first();
		        if ($info) {
			        return [$info->id => $info->contract_no];
		        }
	        })->ajax('/api/getbuildcontract')
		      ->placeholder('请输入合同编号')->required()
		      ->loadinfo('api/getbuildcontract_info',['company_name','project']);
	        });
            
            $form->text('company_name');
            $form->text('project');
```



###### 用法:上方select() /api/getbuildcontract返回当前的id 通过当前的id 调用 另一个接口

##### 接口定义需规范  q接受查询参数  select 返回对应的 上方字段名称

```PHP
public function getbuildcontract_info(Request $request){
		$q = $request->get('q');
		return \App\Models\Buildcontract::where('id', '=', "$q")->select(['company_name','project'])->first();
	}
```

```php
loadinfo('api/getbuildcontract_info',['company_name','project'])
loadinfo('请求路径',['字段名','字段名'])；//可写多个字段名渲染多个input输入框
```

### 效果图

![readme.img](readme.img.png)



### 使用方式图

![输入图片说明](image.png)

